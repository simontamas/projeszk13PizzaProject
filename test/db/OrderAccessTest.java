package db;

import entity.Order;
import entity.PizzaType;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author simontamas
 */
public class OrderAccessTest {
    
    public OrderAccessTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getInstance method, of class OrderAccess.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        OrderAccess result = OrderAccess.getInstance();
        assertNotNull(result);
    }

    /**
     * Test of getAllOrders method, of class OrderAccess.
     */
    @Test
    public void testGetAllOrders() {
        System.out.println("getAllOrders");
        List<Order> result = OrderAccess.getInstance().getAllOrders();
        //assertEquals(result.size(), 5);
        assertNotNull(result);
    }

    /**
     * Test of findOrderById method, of class OrderAccess.
     */
    @Test
    public void testFindOrderById() {
        System.out.println("findOrderById");
        int id = 2;
        Order order = OrderAccess.getInstance().findOrderById(id);
        assertEquals(order.getId(), id);
        assertEquals(order.getClientName(), "Mari");
        assertEquals(order.getQuantity(), 1);
        assertEquals(order.getLocation(), "ErzsébetVáros");
        assertEquals(order.getPizzaType(), PizzaType.SALAMI);
        assertEquals(order.isDelivered(), false);
    }

    /**
     * Test of insertOrder method, of class OrderAccess.
     */
    @Test
    public void testInsertOrder() {
        System.out.println("insertOrder");

        int id = 6;
        String clientName = "Gyurika";
        String location = "Kobanya";
        int quantity = 2;
        PizzaType pizzaType = PizzaType.PIEDONE;
        
        Order order = new Order(id, clientName, location, quantity, pizzaType, location, false);
        OrderAccess.getInstance().insertOrder(order);
        
        assertEquals(order.getId(), id);
        assertEquals(order.getClientName(), clientName);
        assertEquals(order.getQuantity(), quantity);
        assertEquals(order.getLocation(), location);
        assertEquals(order.getPizzaType(), pizzaType);
        assertEquals(order.isDelivered(), false);
    }

    /**
     * Test of updateOrder method, of class OrderAccess.
     */
    @Test
    public void testUpdateOrder() {
        System.out.println("updateOrder");
        Order order = OrderAccess.getInstance().findOrderById(5);

        order.setDelivered(true);
        
        OrderAccess.getInstance().updateOrder(order);
        
        Order updatedOrder = OrderAccess.getInstance().findOrderById(6);
        
        assertEquals(order.isDelivered(), true);

    }
    
}
