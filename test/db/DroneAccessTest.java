package db;

import entity.Drone;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author simontamas
 */
public class DroneAccessTest {
    
    public DroneAccessTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getInstance method, of class DroneAccess.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        DroneAccess result = DroneAccess.getInstance();
        assertNotNull(result);
    }

    /**
     * Test of getAllDrones method, of class DroneAccess.
     */
    @Test
    public void testGetAllDrones() {
        System.out.println("getAllDrones");
        List<Drone> result = DroneAccess.getInstance().getAllDrones();
        assertNotNull(result);
        assertEquals(result.size(), 5);
    }

    /**
     * Test of findDroneById method, of class DroneAccess.
     */
    @Test
    public void testFindDroneById() {
        System.out.println("findDroneById");
        int id = 1;
        Drone drone = DroneAccess.getInstance().findDroneById(id);
        assertNotNull(drone);
        assertEquals(drone.getId(), id);
    }

    /**
     * Test of updateDrone method, of class DroneAccess.
     */
    @Test
    public void testUpdateDrone() {
        System.out.println("updateDrone");
        int id = 1;
        Drone drone = DroneAccess.getInstance().findDroneById(id);
        
        String location = "NON_HQ";
        drone.setLocation(location);
        
        DroneAccess.getInstance().updateDrone(drone);
        
        Drone updatedDrone = DroneAccess.getInstance().findDroneById(id);
        
        assertNotNull(updatedDrone);
        assertEquals(updatedDrone.getId(), id);
        assertEquals(updatedDrone.getLocation(), location);
        
        drone.setLocation("HQ");
        DroneAccess.getInstance().updateDrone(drone);
    }
    
}
