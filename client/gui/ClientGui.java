package client.gui;

import javax.swing.JOptionPane;

import client.gui.spinner.NumberSpinner;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import client.logic.Client;

public class ClientGui extends Application {

	private TextField nameTF;
	private ComboBox<String> locationsCB;
	private ComboBox<String> pizzaTypeCB;
	private NumberSpinner amountNS;
	private SequentialTransition seqTr = null;
	private ImageView loadingV;
	private Image droneLogo;

	static String[] places;
	static String[] pizzas;

	public ClientGui() {
	}

	public ClientGui(String[] pl, String[] pz) {
		super();
		places = pl;
		pizzas = pz;
		seqTr = new SequentialTransition();
	}

	@Override
	public void start(Stage stage) throws Exception {

		initStage(stage);

		GridPane mainGrid = new GridPane();
		mainGrid.setId("main_grid");

		// TITLE
		Label titleL = new Label("Order");
		titleL.setId("titleL");
		mainGrid.add(titleL, 0, 0);

		// ICON
		ImageView logoV = initLogo();
		mainGrid.add(logoV, 1, 0);

		// NAME
		Label nameL = initNameLabel();
		nameTF = initNameTextField();
		mainGrid.add(nameL, 0, 1);
		mainGrid.add(nameTF, 1, 1);

		// LOCATIONS
		ObservableList<String> locations = initLocations();
		Label locationL = initLocationLabel();
		locationsCB = initLocationComboBox(locations);
		mainGrid.add(locationL, 0, 2);
		mainGrid.add(locationsCB, 1, 2);

		// PIZZA TYPES
		ObservableList<String> pizzaTypes = initPizzaTypes();
		Label pizzaTypeL = initPizzaTypeLabel();
		pizzaTypeCB = initPizzaTypeComboBox(pizzaTypes);
		mainGrid.add(pizzaTypeL, 0, 3);
		mainGrid.add(pizzaTypeCB, 1, 3);

		// AMOUNT OF PIZZAS
		Label amountL = initAmountLabel();
		amountNS = initAmountNumberSpinner();
		mainGrid.add(amountL, 0, 4);
		mainGrid.add(amountNS, 1, 4);

		// ORDER
		Button orderB = initOrderButton();
		mainGrid.add(orderB, 1, 5);

		// PIZZA_ANIMATION
		VBox animVB = new VBox();
		droneLogo = new Image("client/gui/images/pizzadrone.png");
		loadingV = new ImageView();
		loadingV.setImage(droneLogo);
		animVB.getChildren().add(loadingV);
		mainGrid.add(animVB, 0, 5);

		TranslateTransition deliverPizzaTr = deliverPizzaTransition();

		TranslateTransition comeBackDroneTr = comeBackDroneTransition();

		ScaleTransition scaleDroneTr = scale1Transition();

		ScaleTransition scaleDroneTr2 = scale2Transition();

		ParallelTransition parTr1 = new ParallelTransition();
		parTr1.getChildren().addAll(deliverPizzaTr, scaleDroneTr);

		ParallelTransition parTr2 = new ParallelTransition();
		parTr2.getChildren().addAll(comeBackDroneTr, scaleDroneTr2);

		ChangeImageTransition chImTr = new ChangeImageTransition(
				Duration.millis(1), loadingV, droneLogo);

		ParallelTransition parTr3 = new ParallelTransition();
		parTr3.getChildren().addAll(chImTr);

		sequentialTransition(parTr1, parTr2, parTr3);
		new Thread(new Runnable() {

			@Override
			public void run() {
				seqTr.play();
			}
		}).start();

		// HOLDER
		VBox holder = new VBox();
		holder.setSpacing(40);
		holder.setId("holder_pane");
		holder.getChildren().add(mainGrid);

		// SCENE
		setScene(stage, holder);
		stage.show();

	}

	private void sequentialTransition(ParallelTransition parTr1,
			ParallelTransition parTr2, ParallelTransition parTr3) {
		seqTr = new SequentialTransition();
		seqTr.setCycleCount(1);
		seqTr.setAutoReverse(true);
		seqTr.setInterpolator(Interpolator.LINEAR);
		seqTr.getChildren().addAll(parTr1, parTr3, parTr2);
	}

	private ScaleTransition scale2Transition() {
		ScaleTransition scaleDroneTr2 = new ScaleTransition(
				Duration.millis(1000), loadingV);
		scaleDroneTr2.setFromX(1);
		scaleDroneTr2.setFromY(1);
		scaleDroneTr2.setToX(2);
		scaleDroneTr2.setToY(2);
		scaleDroneTr2.setCycleCount(Timeline.INDEFINITE);
		scaleDroneTr2.setAutoReverse(true);
		scaleDroneTr2.setDelay(Duration.millis(500));
		return scaleDroneTr2;
	}

	private ScaleTransition scale1Transition() {
		ScaleTransition scaleDroneTr = new ScaleTransition(
				Duration.millis(1000), loadingV);
		scaleDroneTr.setFromX(1);
		scaleDroneTr.setFromY(1);
		scaleDroneTr.setToX(2);
		scaleDroneTr.setToY(2);
		scaleDroneTr.setCycleCount(Timeline.INDEFINITE);
		scaleDroneTr.setAutoReverse(true);
		scaleDroneTr.setDelay(Duration.millis(1000));
		return scaleDroneTr;
	}

	private TranslateTransition comeBackDroneTransition() {
		TranslateTransition comeBackDroneTr = new TranslateTransition(
				Duration.millis(2000), loadingV);
		comeBackDroneTr.setToX(0);
		comeBackDroneTr.setCycleCount(Timeline.INDEFINITE);
		comeBackDroneTr.setAutoReverse(true);
		comeBackDroneTr.setDelay(Duration.millis(500));
		return comeBackDroneTr;
	}

	private TranslateTransition deliverPizzaTransition() {
		TranslateTransition deliverPizzaTr = new TranslateTransition(
				Duration.millis(2000), loadingV);
		deliverPizzaTr.setToX(170);
		deliverPizzaTr.setCycleCount(Timeline.INDEFINITE);
		deliverPizzaTr.setAutoReverse(true);
		deliverPizzaTr.setDelay(Duration.millis(1000));
		return deliverPizzaTr;
	}

	private ImageView initLogo() {
		Image logoI = new Image("client/gui/images/icon.png");
		ImageView logoV = new ImageView();
		logoV.setImage(logoI);
		logoV.setFitHeight(150);
		logoV.setFitWidth(150);
		return logoV;
	}

	private void setScene(Stage stage, VBox holder) {
		Scene scene = new Scene(holder, 640, 500);
		scene.getStylesheets().add("client/gui/stylesheets/client_gui.css");
		stage.setScene(scene);
	}

	private Button initOrderButton() {
		Button orderB = new Button("Order");
		orderB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				handleOrder();
			}
		});
		return orderB;
	}

	private void handleOrder() {
		if (nameTF.getText().equals("")) {
			System.out.println("NO NAME!");
			createPopUp("You can not leave the Name field empty.", false);
		} else {
			String amountSt = "";
			amountSt = amountNS.getNumber().toString();

			Client.makeOrder(nameTF.getText(), locationsCB.getSelectionModel()
					.getSelectedItem(), Integer.parseInt(amountSt), pizzaTypeCB
					.getSelectionModel().getSelectedItem());
			System.out.println("Order: " + nameTF.getText() + "; "
					+ locationsCB.getSelectionModel().getSelectedItem() + "; "
					+ pizzaTypeCB.getSelectionModel().getSelectedItem() + "; "
					+ amountNS.getNumber() + ".");
		}
	}

	private NumberSpinner initAmountNumberSpinner() {
		NumberSpinner amountNS = new NumberSpinner();
		amountNS.setMinValue(0);
		amountNS.setMaxValue(15);
		return amountNS;
	}

	private Label initAmountLabel() {
		Label amountL = new Label("Amount of pizzas: ");
		return amountL;
	}

	private ComboBox<String> initPizzaTypeComboBox(
			ObservableList<String> pizzaTypes) {
		ComboBox<String> pizzaTypeCB = new ComboBox<String>();
		pizzaTypeCB.setItems(pizzaTypes);
		pizzaTypeCB.getSelectionModel().select(0);
		return pizzaTypeCB;
	}

	private Label initPizzaTypeLabel() {
		Label pizzaTypeL = new Label("Pizza type: ");
		return pizzaTypeL;
	}

	private ObservableList<String> initPizzaTypes() {
		ObservableList<String> pizzaTypes = FXCollections.observableArrayList();

		for (int i = 0; i < pizzas.length; ++i) {
			pizzaTypes.add(pizzas[i]);
		}

		return pizzaTypes;
	}

	private ComboBox<String> initLocationComboBox(
			ObservableList<String> locations) {
		ComboBox<String> locationsCB = new ComboBox<String>();
		locationsCB.setItems(locations);
		locationsCB.getSelectionModel().select(0);
		return locationsCB;
	}

	private Label initLocationLabel() {
		Label locationL = new Label("District: ");
		return locationL;
	}

	private ObservableList<String> initLocations() {
		ObservableList<String> locations = FXCollections.observableArrayList();

		for (int i = 0; i < places.length; ++i) {
			locations.add(places[i]);
		}

		return locations;
	}

	private TextField initNameTextField() {
		TextField nameTF = new TextField();
		nameTF.setFocusTraversable(false);
		nameTF.setPromptText("Give us your name");
		return nameTF;
	}

	private Label initNameLabel() {
		Label nameL = new Label("Name: ");
		return nameL;
	}

	private void initStage(Stage stage) {
		stage.setTitle("PizzaDrone Order");
		stage.setResizable(false);
	}

	public void showWindow() {
		launch();
	}

	public void setWindowAccepted() {
		// seqTr.play();
		System.out.println("folyamatban...");
	}

	public void setWindowAccomplished() {
		System.out.println("megj�tt!!");
		createPopUp("The order has been delivered.", true);
	}

	private void createPopUp(String info, boolean exit) {
		Stage stage = new Stage();
		VBox box = new VBox();
		box.setPadding(new Insets(20.0d));
		box.setSpacing(20.0d);
		Label label = new Label(info);
		if (exit)
			label.setId("info_green");
		else
			label.setId("info_red");
		Button okB = new Button("OK");
		box.setAlignment(Pos.CENTER);
		// okB.setAlignment(Pos.CENTER);
		okB.setPrefWidth(150);
		okB.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				if (exit)
					closeWindow();
				else
					stage.close();
			}
		});
		box.getChildren().addAll(label, okB);
		stage.setScene(new Scene(box));
		stage.getScene().getStylesheets()
				.add("client/gui/stylesheets/client_gui.css");
		if (exit)
			stage.setTitle("Completed");
		else
			stage.setTitle("Empty name");
		// stage.initStyle(StageStyle.UTILITY);
		stage.setResizable(false);
		stage.setWidth(350);
		stage.initModality(Modality.APPLICATION_MODAL);
		if (exit)
			stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

				@Override
				public void handle(WindowEvent arg0) {
					closeWindow();
				}
			});
		stage.show();
	}

	private void closeWindow() {
		Platform.exit();
	}

	public void errorWindow(int errNum) {
		switch (errNum) {
		case 1:
			JOptionPane.showMessageDialog(null,
					"Can not find the configuration file.");
			break;
		case 2:
			JOptionPane.showMessageDialog(null, "Unknown host.");
			break;
		case 3:
			JOptionPane.showMessageDialog(null,
					"Can not connect to the server.");
			break;
		case 4:
			JOptionPane.showMessageDialog(null, "IOException.");
			break;
		default:
			break;
		}

	}
}
