package client.gui;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class ChangeImageTransition extends Transition {
	
	private ImageView iv;
	private Image im;

	public ChangeImageTransition(Duration duration, ImageView iv, Image im) {
		this.iv = iv;
		this.im = im;
		setCycleDuration(duration);
		setInterpolator(Interpolator.LINEAR);
	}

	@Override
	protected void interpolate(double k) {
		iv.setImage(im);
	}

}
