package client.logic;

import client.gui.*;

import java.net.*;
import java.io.*;
import java.util.*;

public class Client {

    static String[] places;
    static String[] pizzas;
    static ClientGui cg;
    static Socket client = null;

    public Client() {
    }

    public String[] getPlaces() {
        return places;
    }

    public String[] getPizzas() {
        return pizzas;
    }

    public static void main(String[] args) {
        String fileName = "config.txt";
        Scanner fileReader;
        String host = "";
        int port = 0;
        try {
            fileReader = new Scanner(new FileReader(fileName));
            host = fileReader.nextLine();
            port = Integer.parseInt(fileReader.nextLine());
        } catch (FileNotFoundException ex) {
            cg = new ClientGui();
            cg.errorWindow(1); //1 = nem található a fájl
            System.exit(0);
        }
        
        try {
            client = new Socket(host, port);
        } catch (UnknownHostException e) {
            cg = new ClientGui(); //2 = ismeretlen Ip cím
            cg.errorWindow(2);
            System.exit(0);
        } catch (ConnectException | IllegalArgumentException e) {
            cg = new ClientGui(); //3 = kapcsolati hiba
            cg.errorWindow(3);
            System.exit(0);
        } catch (IOException e) { 
            cg = new ClientGui();
            cg.errorWindow(4); //4 = nem sikerült kapcsolatot létesíteni a szerverrel
            System.exit(0);
        }
        
        try{
            Scanner br = new Scanner(client.getInputStream());
            PrintWriter pw = new PrintWriter(client.getOutputStream());
            
            String message = "";
            message = br.nextLine();
            pw.write("ok\n");
            pw.flush();
            places = message.split(" ");
			
            message = br.nextLine();
            pizzas = message.split(" ");
        } catch (IOException e) {
            System.err.println("IO hiba a csatornak lekerese vagy a kommunikacio kozben.");
            System.err.println(e.getMessage());
            System.exit(0);
        }
        cg = new ClientGui(places,pizzas);
        cg.showWindow();
    }

    public static void makeOrder(String name, String place, int amount, String pizzatype) {
        try{
            PrintWriter pw = new PrintWriter(client.getOutputStream());
            Scanner br = new Scanner(client.getInputStream());
            String message = "";
            message = name; //A rendelő neve
            pw.println(message);
            pw.flush();
            
            message = place; //A rendelés helye
            pw.println(message);
            pw.flush();
			
            message = Integer.toString(amount); //A rendelt mennyiség
            pw.println(message);
            pw.flush();
			
            message = pizzatype; //A rendelt pizza
            pw.println(message);
            pw.flush();
			
            br.nextLine(); //Rendelés fogadva
            cg.setWindowAccepted();
            br.nextLine(); //Rendelés teljesítve.
            cg.setWindowAccomplished();
    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}