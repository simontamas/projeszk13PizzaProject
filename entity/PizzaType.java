package entity;

/**
 *
 * @author simontamas
 */
public enum PizzaType {
    MARGHERITA, SALAMI, MEXICAN, PIEDONE, MUSHROOM, HAWAII, NONE
}
