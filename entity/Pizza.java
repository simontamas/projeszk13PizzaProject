package entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simontamas
 */
public class Pizza {
    private String name; //a pizza neve
    private PizzaType type; //a pizza típusa
    
    public Pizza(PizzaType type){
        this.type = type;
        
        switch(type){
            case MARGHERITA:
                name = "Sajtos pizza";
                break;
            case MEXICAN:
                name = "Mexikói pizza";
                break;
            case MUSHROOM:
                name = "Gombás pizza";
                break;
            case PIEDONE:
                name = "Piedone pizza";
                break;
            case SALAMI:
                name = "Szalámis pizza";
                break;
        }
    }
    
    public String getName(){
        return name;
    }
    
    public PizzaType getType(){
        return type;
    }
    
    /**
     * Pizzák listájának meghatározása.
     * @return 
     */
    public static List<Pizza> getPizzas(){
        ArrayList<Pizza> list = new ArrayList<>();
        
        list.add(new Pizza(PizzaType.MARGHERITA));
        list.add(new Pizza(PizzaType.MEXICAN));
        list.add(new Pizza(PizzaType.MUSHROOM));
        list.add(new Pizza(PizzaType.PIEDONE));
        list.add(new Pizza(PizzaType.SALAMI));
        
        return list;
    }
}
