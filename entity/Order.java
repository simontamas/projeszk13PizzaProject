package entity;

/**
 *
 * @author simontamas
 */
public class Order {

    private int id;
    private String clientName;
    private String location;
    private int quantity;
    private PizzaType pizzaType;
    private String date;
    private boolean delivered;

    public Order() {
    }

    public Order(int id, String clientName, String location, int quantity, PizzaType pizzaType, String date, boolean delivered) {
        this.id = id;
        this.clientName = clientName;
        this.location = location;
        this.quantity = quantity;
        this.pizzaType = pizzaType;
        this.date = date;
        this.delivered = delivered;
    }

    public String getLocation() {
        return location;
    }

    public String getClientName() {
        return clientName;
    }

    public int getId() {
        return id;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDate() {
        return date;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", clientName=" + clientName + ", location=" + location + ", quantity=" + quantity + ", pizzaType=" + pizzaType + '}';
    }
}
