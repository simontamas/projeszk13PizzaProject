package db;

import entity.Order;
import entity.PizzaType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simontamas
 */
public class OrderAccess {

    private static final String FIND_ALL_ORDERS_SQL = "SELECT * FROM Orders";
    private static final String FIND_ORDER_BY_ID_SQL = "SELECT * FROM Orders WHERE id = ?";
    private static final String INSERT_ORDER_SQL = "INSERT INTO Orders (id, client_name, location, quantity, pizza_type, order_date, delivered) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_ORDER_SQL = "UPDATE Orders SET delivered = ? where id = ?";

    private List<Order> orders;
    private static OrderAccess oa;

    private OrderAccess() {
        orders = new ArrayList<>();
    }

    public static OrderAccess getInstance() {
        if (oa == null) {
            oa = new OrderAccess();
        }
        return oa;
    }

    /**
     * Az összes rendelés kilistázása.
     * @return 
     */
    public List<Order> getAllOrders() {

        try {
            orders.clear();
            ResultSet rs = DBManager.getInstance().executeSelect(FIND_ALL_ORDERS_SQL);
            
            while (rs.next()) {
                Order order = orderFromResult(rs);
                orders.add(order);
            }

            return new ArrayList<>(orders);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Egy megadott id-vel rendelkező rendelés meghatározása.
     * @param id
     * @return 
     */
    public Order findOrderById(int id) {
        try {
            ResultSet rs = DBManager.getInstance().executeSelect(FIND_ORDER_BY_ID_SQL, id);
            if (rs.next()) {
                Order order = orderFromResult(rs);

                return order;
            }
        } catch (SQLException ex) {
            throw new RuntimeException("");
        }

        return null;
    }

    /**
     * Rendelés beszúrása az adatbázisba.
     * @param order 
     */
    public void insertOrder(Order order) {
        try {
            DBManager.getInstance().executeOrderInsert(INSERT_ORDER_SQL, order.getId(), order.getClientName(), order.getLocation(), order.getQuantity(), order.getPizzaType().toString(), order.getDate(), order.isDelivered());
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Rendelés frissítése az adatbázisban. 
     * FONTOS: Csak a 'delivered' mező kerül frissítésre.
     * 
     * Használat: mielőtt frissítjük a megrendelést, a paraméterként kapott
     * order objektumnak meghívjuk a setDelivered(true) függvényét.
     * @param order 
     */
    public void updateOrder(Order order) {
        try {
            DBManager.getInstance().executeUpdate(UPDATE_ORDER_SQL, order.isDelivered(), order.getId());
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private Order orderFromResult(ResultSet rs) throws SQLException {
        Order order = new Order();
        order.setClientName(rs.getString("client_name"));
        order.setId(Integer.parseInt(rs.getString("id")));
        order.setLocation(rs.getString("location"));
        order.setQuantity(Integer.parseInt(rs.getString("quantity")));
        order.setDate(rs.getString("order_date"));
        order.setDelivered(rs.getBoolean("delivered"));

        setPizzaTypeFromString(order, rs.getString("pizza_type"));

        return order;
    }

    private void setPizzaTypeFromString(Order order, String typeString) {
        if (typeString.equals(PizzaType.MARGHERITA.toString())) {
            order.setPizzaType(PizzaType.MARGHERITA);
        } else if (typeString.equals(PizzaType.MEXICAN.toString())) {
            order.setPizzaType(PizzaType.MEXICAN);
        } else if (typeString.equals(PizzaType.MUSHROOM.toString())) {
            order.setPizzaType(PizzaType.MUSHROOM);
        } else if (typeString.equals(PizzaType.PIEDONE.toString())) {
            order.setPizzaType(PizzaType.PIEDONE);
        } else if (typeString.equals(PizzaType.SALAMI.toString())) {
            order.setPizzaType(PizzaType.SALAMI);
        }else if (typeString.equals(PizzaType.HAWAII.toString())) {
            order.setPizzaType(PizzaType.HAWAII);
        }else {
            order.setPizzaType(PizzaType.NONE);
        }
    }
}
