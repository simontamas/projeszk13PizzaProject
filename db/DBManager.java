package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author situaai
 */
public class DBManager {

    private final String dbName = "pizzadb";
    private final String user = "projeszkteam13";
    private final String pwd = "projeszkteam13";
    private final String dbms = "derby";
    private final String host = "localhost";
    private final int port = 1527;

    private static DBManager db;

    private Connection conn;

    private DBManager() {
        try {
            connect();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static DBManager getInstance() {
        if (db == null) {
            db = new DBManager();
        }
        return db;
    }

    private void connect() throws Exception {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        conn = DriverManager.getConnection(
                "jdbc:" + dbms + ":" + "//" + host + ":" + port + "/" + dbName, user, pwd
        );
    }

    public void closeConnection() {
        if (conn == null) {
            return;
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public ResultSet executeSelect(String sql, Object... args) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(sql);
        for (int i = 0; i < args.length; i++) {
            stmt.setObject(i + 1, args[i]);
        }
        return stmt.executeQuery();
    }

    public void executeUpdate(String sql, Object... args) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(sql);

        stmt.setObject(1, args[0]);
        stmt.setObject(2, args[1]);

        stmt.executeUpdate();
    }

    public void executeOrderInsert(String sql, Object... args) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(sql);

        stmt.setObject(1, args[0]);
        stmt.setObject(2, args[1]);
        stmt.setObject(3, args[2]);
        stmt.setObject(4, args[3]);
        stmt.setObject(5, args[4]);
        stmt.setObject(6, args[5]);
        stmt.setObject(7, args[6]);

        stmt.executeUpdate();
    }
}
