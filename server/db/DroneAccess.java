package db;

import entity.Drone;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simontamas
 */
public class DroneAccess {

    private static final String FIND_ALL_DRONES_SQL = "SELECT * FROM Drones";
    private static final String FIND_DRONE_BY_ID_SQL = "SELECT * FROM Drones WHERE id = ?";
    private static final String UPDATE_DRONE_SQL = "UPDATE Drones SET location = ? where id = ?";

    private List<Drone> drones;
    private static DroneAccess da;

    private DroneAccess() {
        drones = new ArrayList<>();
    }

    public static DroneAccess getInstance() {
        if (da == null) {
            da = new DroneAccess();
        }

        return da;
    }

    public List<Drone> getAllDrones() {
        try {
            drones.clear();

            ResultSet rs = DBManager.getInstance().executeSelect(FIND_ALL_DRONES_SQL);
            while (rs.next()) {
                Drone drone = droneFromResult(rs);
                drones.add(drone);
            }

            return drones;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Egy megadott id-vel rendelkező rendelés meghatározása.
     *
     * @param id
     * @return
     */
    public Drone findDroneById(int id) {
        try {
            ResultSet rs = DBManager.getInstance().executeSelect(FIND_DRONE_BY_ID_SQL, id);
            if (rs.next()) {
                Drone drone = droneFromResult(rs);

                return drone;
            }
        } catch (SQLException ex) {
            throw new RuntimeException("");
        }

        return null;
    }

    /**
     * Kiszállító egység frissítése az adatbázisban. 
     * FONTOS: Csak a 'location' mező kerül frissítésre.
     *
     * Használat: mielőtt frissítjük a drónt, a paraméterként kapott
     * drone objektumnak meghívjuk a setLocation(str) függvényét.
     *
     * @param drone
     */
    public void updateDrone(Drone drone) {
        try {
            DBManager.getInstance().executeUpdate(UPDATE_DRONE_SQL, drone.getLocation(), drone.getId());
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private Drone droneFromResult(ResultSet rs) throws SQLException {
        Drone drone = new Drone();

        drone.setId(Integer.parseInt(rs.getString("id")));
        drone.setLocation(rs.getString("location"));

        return drone;
    }
}
