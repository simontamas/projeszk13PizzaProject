package gui.server;

import db.*;
import entity.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.List;
import javax.swing.*;
import server.Server;

/**
 *
 * @author adam
 */
public class ServerGui extends JFrame {

    private final JPanel topPanel;
    private final JTable droneTable;
    private final JScrollPane droneScrollPane;
    private final JTable orderTable;
    private final JScrollPane orderScrollPane;
    private final Server serverLogic;
    private final DroneTableModel droneTableModel;
    private final OrderTableModel orderTableModel;
    private JTextArea statusbar;
    private JScrollPane statusScrollPane;

    public ServerGui(Server serverLogic) {
        this.setTitle("Server");
        this.setLocation(20, 20);
        this.setSize(new Dimension(800, 640));
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setBackground(Color.gray);

        
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);

        this.serverLogic = serverLogic;
        
        droneTableModel = new DroneTableModel(serverLogic);
        droneTable = new JTable(droneTableModel);
        droneScrollPane = new JScrollPane(droneTable);
        
        orderTableModel = new OrderTableModel(serverLogic);
        orderTable = new JTable(orderTableModel);
        orderScrollPane = new JScrollPane(orderTable);
        
        
        JTabbedPane jTP = new JTabbedPane();
        jTP.addTab("Orders", orderScrollPane);
        jTP.setSelectedIndex(0);
        jTP.addTab("Drones", droneScrollPane);
        
        topPanel.add(jTP, BorderLayout.NORTH);
        
        
        statusbar = new JTextArea(20, 100);
        statusbar.setEditable(false);
        statusScrollPane =  new JScrollPane(statusbar); 
        topPanel.add(statusScrollPane, BorderLayout.SOUTH);
        
        pack();
    }
    
    public void updateStatusBar(String str){
        statusbar.append(str + "\n");
    }
        
    public void update(){
        this.droneTableModel.fireTableDataChanged();
        this.orderTableModel.fireTableDataChanged();
    }

}