package gui.server;

import db.DroneAccess;
import javax.swing.table.AbstractTableModel;
import entity.*;
import java.util.List;
import server.Server;

/**
 *
 * @author adam
 */
public class DroneTableModel extends AbstractTableModel {
    
    private String[] column = { "Drone id", "Location" };
    
    private List<Drone> droneList;
    private final Server serverLogic;
    
    public DroneTableModel(Server serverLogic){
        this.serverLogic = serverLogic;
        this.droneList = serverLogic.getDrones();
    }
    
    @Override
    public int getRowCount() {
        return droneList.size();
    }

    @Override
    public int getColumnCount() {
        return column.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column){
            case 0:
                return droneList.get(row).getId();
            case 1:
                return droneList.get(row).getLocation();
            default:
                return null;
        }
    }
    
    @Override
    public void fireTableDataChanged() {
        droneList = serverLogic.getDrones();
        super.fireTableDataChanged(); 
    }
        

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
    }

    @Override
    public String getColumnName(int i) {
        return column[i];
    }

}