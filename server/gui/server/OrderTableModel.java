package gui.server;

import db.OrderAccess;
import entity.Order;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import server.Server;

/**
 *
 * @author adam
 */
public class OrderTableModel extends AbstractTableModel {

    private String[] column = { "Order id", "Client name", "Location", "Quantity", "Pizza type", "Order date", "Delivered" };
    
    private List<Order> orderList;
    private Server serverLogic;
    
    public OrderTableModel(Server serverLogic){
        this.serverLogic = serverLogic;
        orderList = serverLogic.getOrders();
    }
    
    @Override
    public int getRowCount() {
        return orderList.size();
    }

    @Override
    public int getColumnCount() {
        return column.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column){
            case 0:
                return orderList.get(row).getId();
            case 1:
                return orderList.get(row).getClientName();
            case 2:
                return orderList.get(row).getLocation();
            case 3:
                return orderList.get(row).getQuantity();
            case 4:
                return orderList.get(row).getPizzaType();
            case 5:
                return orderList.get(row).getDate();
            case 6:
                return orderList.get(row).isDelivered();
            default:
                return null;
        }
    }

    @Override
    public void fireTableDataChanged() {
        orderList = serverLogic.getOrders();
        super.fireTableDataChanged(); 
    }
        
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 6:
                Order o = serverLogic.getOA().findOrderById(rowIndex+1);
                o.setDelivered(!o.isDelivered());
                serverLogic.getOA().updateOrder(o);
                fireTableDataChanged();
                break;
            default:
                break;
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
        return column == 6;
    }

    @Override
    public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
    }

    @Override
    public String getColumnName(int i) {
        return column[i];
    }
}
