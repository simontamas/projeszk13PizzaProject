
package server;

import gui.server.ServerGui;
import java.io.IOException;

/**
 *
 * @author LSAG9A
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Server server = new Server();
            ServerGui sG = new ServerGui(server);
            sG.setVisible(true);
            server.linkGui(sG);
        } catch (IOException ex) {
        }
    }
}
