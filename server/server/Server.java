
package server;

import db.DroneAccess;
import db.OrderAccess;
import entity.Drone;
import entity.Order;
import entity.PizzaType;
import gui.server.ServerGui;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author LSAG9A
 */
public class Server {
    
    private int port;
    
    private ServerSocket serverSocket;
    private Socket socket;
    
    static private Scanner scanner;
    static private PrintWriter printWriter;
    
    static private DroneAccess droneA;
    static private OrderAccess orderA;

    public Server() throws IOException {
        
        port = 42420;
        serverSocket = new ServerSocket(port);
        droneA = DroneAccess.getInstance();
        orderA = OrderAccess.getInstance();
        
        start();
        
    }
    
    //String[] tempPlaces = {"JózsefVáros","ErzsébetVáros","TerzésVáros","LipótVáros","SzentImreVáros"};  //Ideiglenes Helyek
    String[] tempPlaces = {"JozsefVaros","ErzsebetVaros","TerzesVaros","LipotVaros","SzentImreVaros"};  //Ideiglenes Helyek

    private void start() throws IOException {        
        
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                
                while (true) {                    
                    try {
                        socket = serverSocket.accept();
                        scanner = new Scanner(socket.getInputStream());
                        printWriter = new PrintWriter(socket.getOutputStream());

                        System.out.println("Scanner and PrintWriter init");

                        ArrayList<String> tPlArray = new ArrayList(Arrays.asList(tempPlaces));

                        printWriter.write(stringPrepForSend(tPlArray));
                        printWriter.flush();

                        System.out.println("Places sent to connected client");

                        scanner.nextLine();

                        ArrayList<String> tPzArray = new ArrayList();

                        ArrayList<PizzaType> pizzaTypeAList = new ArrayList(Arrays.asList(PizzaType.values()));

                        for (PizzaType pizzaType : pizzaTypeAList) {
                            tPzArray.add(pizzaType.toString());
                        }

                        printWriter.write(stringPrepForSend(tPzArray));
                        printWriter.flush();
                        System.out.println("Pizza types sent to connected client");
                        System.out.println("------------------------------------------");
                        String orderHelper;
                        String orderName;
                        String orderLocation;
                        String orderAmount;
                        String orderType;
                        while(true){

                            try {
                                //RendNev
                                System.out.println("waiton for new order");
                                orderHelper = scanner.nextLine();
                                System.out.println("Trying read name - 1");
                                
                                orderName = orderHelper;
                                //orderName = stringDecoder(orderHelper);
                                
                                System.out.println("order name read");
                                sG.updateStatusBar("-------------------------------------");
                                sG.updateStatusBar("Új rendelés a "+orderName+" klienstől");
                                //Rendhely
                                System.out.println("Trying read loc - 2");
                                
                                orderHelper = scanner.nextLine();
                                
                                orderLocation = orderHelper;
                                //orderLocation = stringDecoder(orderHelper);
                                
                                System.out.println("order location read");
                                sG.updateStatusBar("Rendelés célja: "+orderLocation);
                                //rendmeny
                                System.out.println("Trying read amo - 3");
                                
                                orderHelper = scanner.nextLine();
                                
                                orderAmount = orderHelper;
                                //orderAmount = stringDecoder(orderHelper);
                                
                                
                                System.out.println("order amount read");
                                sG.updateStatusBar("Rendelés mennyiség: "+orderAmount);
                                //rendpizz
                                System.out.println("Trying read type");
                                
                                orderHelper = scanner.nextLine();
                                
                                orderType = orderHelper;
                                //orderType = stringDecoder(orderHelper);
                                
                                System.out.println("order type read");
                                sG.updateStatusBar("Rendelés tipus: "+orderType);

                                Order or = new Order();
                                or.setClientName(orderName);
                                int temp = orderA.getAllOrders().get(orderA.getAllOrders().size()-1).getId();
                                temp++;
                                or.setId(temp);
                                or.setLocation(orderLocation);

                                or.setPizzaType(PizzaType.valueOf(orderType));
                                or.setQuantity(Integer.parseInt(orderAmount));

                                String timeStamp = new SimpleDateFormat("yyyy.MM.dd").format(Calendar.getInstance().getTime());

                                or.setDate(timeStamp);

                                //or.setDate();
                                or.setDelivered(false);

                                orderA.insertOrder(or);
                                System.out.println("Setting up Drone");
                                sG.updateStatusBar("Drone előkészítése és elküldése a célra.");
                                Thread tD = new Thread(new RunnableCustom(or));
                                tD.start();
                                //rendfogsend
                                printWriter.write("OK"+System.lineSeparator());
                                printWriter.flush();
                                System.out.println("sent OK");
                                sG.updateStatusBar("Rendelés fogadva ACK.");
                            } catch (NumberFormatException ex) {
                                System.out.println(ex.toString());
                            }

                        }
                    } catch (Exception ex) {
                        sG.updateStatusBar("Kliens kilepett.");
                        sG.updateStatusBar("------------------------------------------");
                        System.out.println("Client close");
                        System.out.println("----------------------------------------");
                    }
                }
                
                
                
            }
        });
        
        t.start();
        
        
        
    }
    
    /**
     * ArrayList-ben található Stringek összefűzése , szóközt használva elválaszáshoz
     * @param stringArray
     * @return 
     */
    private String stringPrepForSend(ArrayList<String> stringArray){
        
        String returnString = "";
        
        for (String string : stringArray) {
            returnString += string + " ";
        }
        returnString = returnString.trim();
        returnString += System.lineSeparator();
        return returnString;
        
    }
    
    /**
     * String kodolása
     * @param msg
     * @return 
     */
    /*
    private String stringCoder(String msg){
        
        String returnString = "";
        
        for (int i = 0; i < msg.length(); i++) {
            char character = msg.charAt(i);
            int ascii = (int) character;
            int temp = (ascii*ascii)-1;
            returnString += temp + " ";
        }
        
        return returnString;
        
    }
    */
    /**
     * String dekodolása
//     * @param msg
     * @return 
     */
//    private String stringDecoder(String msg){
//        System.out.println(msg);
//        String returnString = "";
//        
//        String[] splittedMsg = msg.split(" ");
//        /*
//        for (String string : splittedMsg) {
//            System.out.println(string);
//        }
//        */
//        for (int i = 0; i < splittedMsg.length; i++) {
//            System.out.println(i+" : "+splittedMsg[i]);
//            int temp = Integer.valueOf(splittedMsg[i]);
//            temp++;
//            returnString += (char) Math.sqrt(temp);
//        }
//        
//        return returnString;
//        
//    }
    
    public List<Drone> getDrones(){
        return droneA.getAllDrones();
    }
    
    public List<Order> getOrders(){
        return orderA.getAllOrders();
    }
    
    public DroneAccess getDA(){
        return droneA;
    }
    
    public OrderAccess getOA(){
        return orderA;
    }

    static ServerGui sG;
    
    void linkGui(ServerGui sG) {
        this.sG = sG;
    }

    private static class RunnableCustom implements Runnable {

        Order or;
        
        public RunnableCustom(Order or) {
            this.or = or;
        }

        @Override
        public void run() {
            
            Drone dTemp = null;

            boolean notFound = true;

            while (notFound) {                                
                for (int i = 0; i < droneA.getAllDrones().size(); i++) {
                    if (droneA.getAllDrones().get(i).getLocation().equals("HQ")) {
                        dTemp = droneA.getAllDrones().get(i);
                        notFound = false;
                        break;
                    }
                }
            }

            dTemp.setLocation(or.getLocation());

            droneA.updateDrone(dTemp);

            sG.update();
            
            try {
                //rendteljsend
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }

            dTemp.setLocation("HQ");
            droneA.updateDrone(dTemp);

            or.setDelivered(true);
            orderA.updateOrder(or);
            printWriter.write("Comp"+System.lineSeparator());
            printWriter.flush();
            System.out.println("setn Comp");
            sG.updateStatusBar("Rendelés teljesítve ACK");
            sG.update();
        }
    }
    
}
